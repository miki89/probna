class ClientsController < ApplicationController
  def index
	@clients = Client.all
  end

  def show
	@client = Client.find(params[:id])
	@products = @client.products
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def delete
  end

  def destroy
  end
end
