class Client < ActiveRecord::Base
has_many :parts
has_many :products, through: :parts
end
