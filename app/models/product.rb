class Product < ActiveRecord::Base
has_many :parts
has_many :clients, through: :parts
end
