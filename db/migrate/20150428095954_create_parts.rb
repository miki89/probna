class CreateParts < ActiveRecord::Migration
  def change
    create_table :parts do |t|
	t.belongs_to :client, index: true
	t.belongs_to :product, index: true
      t.timestamps null: false
    end
  end
end
