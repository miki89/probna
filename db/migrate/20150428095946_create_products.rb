class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
	t.string :title
	t.string :image
	t.string :release_year
	t.string :description
      t.timestamps null: false
    end
  end
end
