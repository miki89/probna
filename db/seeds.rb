# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

a1 = Client.create(name: "Pierre", image: "http://www.ibiblio.org/wm/paint/auth/renoir/renoir.jpg", bio: "Francuskie pieski tańczą postimpresjonistycznie")
a2 = Client.create(name: "Rubens", image: "http://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Sir_Peter_Paul_Rubens_-_Portrait_of_the_Artist_-_Google_Art_Project.jpg/220px-Sir_Peter_Paul_Rubens_-_Portrait_of_the_Artist_-_Google_Art_Project.jpg", bio: "Holenderska bajka o kopciuszku")
a3 = Client.create(name: "Filip", image: "http://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Philip_IV_of_Spain_-_Vel%C3%A1zquez_1644.jpg/231px-Philip_IV_of_Spain_-_Vel%C3%A1zquez_1644.jpg", bio: "Angielska robota włoskich imigrantów")
a4 = Client.create(name: "Diego", image: "http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Diego_Vel%C3%A1zquez_Autorretrato_45_x_38_cm_-_Colecci%C3%B3n_Real_Academia_de_Bellas_Artes_de_San_Carlos_-_Museo_de_Bellas_Artes_de_Valencia.jpg/220px-Diego_Vel%C3%A1zquez_Autorretrato_45_x_38_cm_-_Colecci%C3%B3n_Real_Academia_de_Bellas_Artes_de_San_Carlos_-_Museo_de_Bellas_Artes_de_Valencia.jpg", bio: "Boski Diego hiszpańską inkwizycją")
a5 = Client.create(name: "Salvador", image: "http://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Salvador_Dal%C3%AD_1939.jpg/220px-Salvador_Dal%C3%AD_1939.jpg", bio: "Salwador ale nie z Salwadoru")
a6 = Client.create(name: "Edouard", image: "http://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/%C3%89douard_Manet-crop.jpg/220px-%C3%89douard_Manet-crop.jpg", bio: "Eduard francuski nim impresjonista")
a7 = Client.create(name: "Pablo", image:"http://upload.wikimedia.org/wikipedia/en/thumb/4/41/Portrait_of_Pablo_Picasso%2C_1908-1909%2C_anonymous_photographer%2C_Mus%C3%A9e_Picasso%2C_Paris...jpg/220px-Portrait_of_Pablo_Picasso%2C_1908-1909%2C_anonymous_photographer%2C_Mus%C3%A9e_Picasso%2C_Paris...jpg", bio: "Hiszpański Pablo co malował")
a8 = Client.create(name: "Henri", image: "http://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Portrait_of_Henri_Matisse_1933_May_20.jpg/220px-Portrait_of_Henri_Matisse_1933_May_20.jpg", bio: "Dziwny jest ten świat")
a9 = Client.create(name: "Marcel", image: "http://upload.wikimedia.org/wikipedia/en/thumb/9/95/Marcel_Duchamp_playing_chess_%28photo_by_Kay_Bell_Reynal%2C_1952%29.jpg/220px-Marcel_Duchamp_playing_chess_%28photo_by_Kay_Bell_Reynal%2C_1952%29.jpg", bio: "Czarno-biała wstęga codzienności")
a10 = Client.create(name: "Gertruda", image: "http://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Gertrude_Stein_1935-01-04.jpg/200px-Gertrude_Stein_1935-01-04.jpg", bio: "Nie jestem feministką")


p1 = Product.create(title: "Woman with hat", image: "http://upload.wikimedia.org/wikipedia/en/thumb/f/fb/Matisse-Woman-with-a-Hat.jpg/300px-Matisse-Woman-with-a-Hat.jpg", release_year: "1900", description: "Womanizer with hatty hat")
p2 = Product.create(title: "Luxembourg gardens", image: "http://upload.wikimedia.org/wikipedia/en/thumb/b/bf/Matisse_-_Luxembourg_Gardens_%281901%29.jpg/160px-Matisse_-_Luxembourg_Gardens_%281901%29.jpg", release_year: "1945", description: "Small country")
p3 = Product.create(title: "Carmelita", image: "http://upload.wikimedia.org/wikipedia/en/thumb/a/ac/Henri_Matisse%2C_1904%2C_Nu_%28Carmelita%29%2C_oil_on_canvas%2C_81.3_x_59_cm%2C_Museum_of_Fine_Arts%2C_Boston.jpg/112px-Henri_Matisse%2C_1904%2C_Nu_%28Carmelita%29%2C_oil_on_canvas%2C_81.3_x_59_cm%2C_Museum_of_Fine_Arts%2C_Boston.jpg", release_year: "1920", description: "Naked gun")
p4 = Product.create(title: "Selfportrait", image: "http://upload.wikimedia.org/wikipedia/en/thumb/8/8f/Henri_Matisse_Self-Portrait_in_a_Striped_T-shirt_%281906%29.jpg/134px-Henri_Matisse_Self-Portrait_in_a_Striped_T-shirt_%281906%29.jpg", release_year: "1922", description: "Selfik")
p5 = Product.create(title: "Nudity", image: "http://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Henri_Matisse%2C_1907%2C_La_coiffure%2C_116_x_89_cm%2C_oil_on_canvas%2C_Staatsgalerie%2C_Stuttgart.jpg/122px-Henri_Matisse%2C_1907%2C_La_coiffure%2C_116_x_89_cm%2C_oil_on_canvas%2C_Staatsgalerie%2C_Stuttgart.jpg", release_year: "1933", description: "Naked gun vol 2")
p6 = Product.create(title: "Old woman", image: "http://upload.wikimedia.org/wikipedia/en/thumb/4/40/Pablo_Picasso%2C_1901%2C_Old_Woman_%28Woman_with_Gloves%2C_Woman_With_Jewelery%29%2C_oil_on_cardboard%2C_26_3-8_x_20_1-2_inches_%2867_x_52.1_cm%29%2C_Philadelphia_Museum_of_Art.jpg/170px-Pablo_Picasso%2C_1901%2C_Old_Woman_%28Woman_with_Gloves%2C_Woman_With_Jewelery%29%2C_oil_on_cardboard%2C_26_3-8_x_20_1-2_inches_%2867_x_52.1_cm%29%2C_Philadelphia_Museum_of_Art.jpg", release_year: "1965", description: "Stara wiedźma")
p7 = Product.create(title: "Cafee", image: "http://upload.wikimedia.org/wikipedia/en/thumb/2/23/Pablo_Picasso%2C_1901-02%2C_Femme_au_caf%C3%A9_%28Absinthe_Drinker%29%2C_oil_on_canvas%2C_73_x_54_cm%2C_Hermitage_Museum%2C_Saint_Petersburg%2C_Russia.jpg/170px-Pablo_Picasso%2C_1901-02%2C_Femme_au_caf%C3%A9_%28Absinthe_Drinker%29%2C_oil_on_canvas%2C_73_x_54_cm%2C_Hermitage_Museum%2C_Saint_Petersburg%2C_Russia.jpg", release_year: "1910", description: "Kafejka")
p8 = Product.create(title: "Garcon", image: "http://upload.wikimedia.org/wikipedia/en/thumb/9/9c/Gar%C3%A7on_%C3%A0_la_pipe.jpg/170px-Gar%C3%A7on_%C3%A0_la_pipe.jpg", release_year: "1970", description: "Boy")
p9 = Product.create(title: "Portret Gertrudy", image: "http://upload.wikimedia.org/wikipedia/en/thumb/d/d6/GertrudeStein.JPG/220px-GertrudeStein.JPG", release_year: "1915", description: "Feminizm zwycieżył")
p10 = Product.create(title: "Woman with mandolin", image: "http://upload.wikimedia.org/wikipedia/en/thumb/1/1c/Pablo_Picasso%2C_1910%2C_Girl_with_a_Mandolin_%28Fanny_Tellier%29%2C_oil_on_canvas%2C_100.3_x_73.6_cm%2C_Museum_of_Modern_Art_New_York..jpg/148px-Pablo_Picasso%2C_1910%2C_Girl_with_a_Mandolin_%28Fanny_Tellier%29%2C_oil_on_canvas%2C_100.3_x_73.6_cm%2C_Museum_of_Modern_Art_New_York..jpg", release_year: "1910", description: "Kobieta z mandoliną")

p1.clients << [a1, a3, a7, a8]
p2.clients << [a2, a6, a10]
p3.clients << [a1, a4, a5]
p4.clients << [a9, a3, a2]
p5.clients << [a1, a2, a3]
p6.clients << [a5, a6, a7, a8, a9]
p7.clients << [a4, a6, a3]
p8.clients << [a6]
p9.clients << [a2, a5, a7]
p10.clients << [a8, a9]
